#!/bin/bash
################################################################################
# Libre tools 
# Originated from scripts by by CryptoLions.io
###############################################################################

DATADIR="/opt/libre-chain-nodes/libreNode"

$DATADIR/stop.sh
echo -e "Starting Nodeos \n";

ulimit -n 65535
ulimit -s 64000

nodeos --data-dir $DATADIR --config-dir $DATADIR "$@" > $DATADIR/stdout.txt 2> $DATADIR/stderr.txt &  echo $! > $DATADIR/nodeos.pid

